﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace DistinteBase
{
    [FormAttribute("DistinteBase.DiBa", "DiBa.b1f")]
    class DiBa : UserFormBase
    {
        private SAPbouiCOM.Matrix Matrix0;
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;
        private SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
        private SAPbouiCOM.StaticText StaticText2;
        private SAPbouiCOM.EditText EditText2;
        private SAPbouiCOM.ComboBox ComboBox0;
        private SAPbouiCOM.StaticText StaticText4;
        private SAPbouiCOM.ComboBox ComboBox1;
        private SAPbouiCOM.StaticText StaticText7;
        private SAPbouiCOM.EditText EditText3;
        private SAPbouiCOM.StaticText StaticText8;
        private SAPbouiCOM.StaticText StaticText9;
        private SAPbouiCOM.ComboBox ComboBox4;
        private bool loadFromLink;

        public DiBa()
        {
        }

        public override void OnInitializeComponent()
        {
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_1").Specific));
            this.EditText2.DoubleClickAfter += new SAPbouiCOM._IEditTextEvents_DoubleClickAfterEventHandler(this.EditText2_DoubleClickAfter);
            this.EditText2.ValidateAfter += new SAPbouiCOM._IEditTextEvents_ValidateAfterEventHandler(this.EditText2_ValidateAfter);
            this.EditText3 = ((SAPbouiCOM.EditText)(this.GetItem("Item_4").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_2").Specific));
            this.ComboBox0.ClickBefore += new SAPbouiCOM._IComboBoxEvents_ClickBeforeEventHandler(this.ComboBox0_ClickBefore);
            this.ComboBox1 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_18").Specific));
            this.ComboBox1.ComboSelectAfter += new SAPbouiCOM._IComboBoxEvents_ComboSelectAfterEventHandler(this.ComboBox1_ComboSelectAfter);
            this.ComboBox4 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_20").Specific));
            this.StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_0").Specific));
            this.StaticText4 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_5").Specific));
            this.StaticText7 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_7").Specific));
            this.StaticText8 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.StaticText9 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_19").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_16").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("Item_17").Specific));
            this.Button1.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button1_ClickAfter);
            this.Button1.ClickBefore += new SAPbouiCOM._IButtonEvents_ClickBeforeEventHandler(this.Button1_ClickBefore);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("Item_15").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_8").Specific));
            this.EditText0.ChooseFromListAfter += new SAPbouiCOM._IEditTextEvents_ChooseFromListAfterEventHandler(this.EditText0_ChooseFromListAfter);
            this.EditText0.ValidateAfter += new SAPbouiCOM._IEditTextEvents_ValidateAfterEventHandler(this.EditText0_ValidateAfter);
            this.OnCustomInitialize();

        }

        public override void OnInitializeFormEvents()
        {
            this.LoadAfter += new SAPbouiCOM.Framework.FormBase.LoadAfterHandler(this.Form_LoadAfter);
            this.DataLoadAfter += new SAPbouiCOM.Framework.FormBase.DataLoadAfterHandler(this.Form_DataLoadAfter);
            this.ResizeAfter += new ResizeAfterHandler(this.Form_ResizeAfter);

        }

        private void OnCustomInitialize()
        {
            PopolaComboSKU();
            condizioniCFLBP();
            //PopolaComboCli();
        }

        private void condizioniCFLBP()
        {
            SAPbouiCOM.ChooseFromList oCFLCORR = this.UIAPIRawForm.ChooseFromLists.Item("CFL_BP");
            SAPbouiCOM.Conditions oCons = oCFLCORR.GetConditions();
            SAPbouiCOM.Condition oCon = null;
            oCon = oCons.Add();
            oCon.Alias = "CardType";
            oCon.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
            oCon.CondVal = "C";

            oCFLCORR.SetConditions(oCons);
        }


        private void PopolaComboSKU()
        {
            //Se popolato solo BP e Ordine Cliente
            if (EditText2.Value.ToString() != "" && ComboBox1.Value.ToString() != "")
            {
                while (ComboBox0.ValidValues.Count != 0)
                {
                    ComboBox0.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                }
                ComboBox0.ValidValues.Add("", "");
                string CardCode = EditText2.Value.ToString();
                string OrdCli = ComboBox1.Value.ToString();
                string  Query = " SELECT DISTINCT (T0.\"U_FO_EC_SKU\") ";
                        Query += " FROM OITM T0 INNER JOIN RDR1 T1 ON T0.\"ItemCode\" = T1.\"ItemCode\" ";
                        Query += " WHERE (T0.\"U_FO_EC_SKU\" <> '' OR T0.\"U_FO_EC_SKU\" IS NOT NULL) AND T1.\"DocEntry\" = '"+OrdCli+ "' ORDER BY T0.\"U_FO_EC_SKU\"";

                oRecordset.DoQuery(Query);
                while (!oRecordset.EoF)
                {
                    ComboBox0.ValidValues.Add(oRecordset.Fields.Item(0).Value.ToString(), oRecordset.Fields.Item(0).Value.ToString());
                    oRecordset.MoveNext();
                }
                ComboBox0.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            }
            else
            {
                while (ComboBox0.ValidValues.Count != 0)
                {
                    ComboBox0.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                }
                ComboBox0.ValidValues.Add("", "");
                string CardCode = EditText2.Value.ToString();
                string OrdCli = ComboBox1.Value.ToString();
                string Query = " SELECT DISTINCT (T0.\"U_FO_EC_SKU\") ";
                Query += " FROM OITM T0 WHERE T0.\"U_FO_EC_SKU\" <> '' OR T0.\"U_FO_EC_SKU\" IS NOT NULL ORDER BY T0.\"U_FO_EC_SKU\"";

                oRecordset.DoQuery(Query);
                while (!oRecordset.EoF)
                {
                    ComboBox0.ValidValues.Add(oRecordset.Fields.Item(0).Value.ToString(), oRecordset.Fields.Item(0).Value.ToString());
                    oRecordset.MoveNext();
                }
                ComboBox0.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            }
        }
        
        private void PopolaComboOrdini()
        {
            if (EditText0.Value.ToString() != "" )
            {
                while (ComboBox1.ValidValues.Count != 0)
                {
                    ComboBox1.ValidValues.Remove(0, SAPbouiCOM.BoSearchKey.psk_Index);
                }
                ComboBox1.ValidValues.Add("", "");
                string CardCode = EditText0.Value.ToString();
                oRecordset.DoQuery("SELECT DISTINCT(T0.\"DocNum\"), T0.\"DocEntry\"  FROM ORDR T0 WHERE T0.\"CardCode\" = '"+ CardCode + "' AND  T0.\"DocStatus\"  = 'O' ORDER BY T0.\"DocNum\" DESC ");
                while (!oRecordset.EoF)
                {
                    ComboBox1.ValidValues.Add(oRecordset.Fields.Item(1).Value.ToString(), oRecordset.Fields.Item(0).Value.ToString());
                    oRecordset.MoveNext();
                }
                ComboBox1.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
            }
        }

        private void PopolaMatrixArticoli()
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string HanaQuery = "";
            HanaQuery += "SELECT T0.\"ItemCode\", T0.\"ItemName\", T3.\"Father\", NULL AS \"Qta\", T4.\"UomName\" FROM OITM T0 LEFT JOIN RDR1 T1 ON T0.\"ItemCode\" = T1.\"ItemCode\" ";
            HanaQuery += " LEFT JOIN ORDR T2 ON T1.\"DocEntry\" = T2.\"DocEntry\" LEFT JOIN ITT1 T3 ON T0.\"ItemCode\" = T3.\"Father\" LEFT JOIN OUOM T4 ON T0.\"PriceUnit\" = T4.\"UomEntry\" ";
            HanaQuery += " WHERE 1 = 1 ";
            if (this.ComboBox0.Value.ToString() != "")
            {
                HanaQuery += " AND T0.\"U_FO_EC_SKU\" = '"+ this.ComboBox0.Value.ToString() + "' ";
            }
            if(this.ComboBox1.Value.ToString() != "")
            {
                HanaQuery += " AND T2.\"DocEntry\" = '" + this.ComboBox1.Value.ToString() + "' ";
            }
            HanaQuery += " ";
            HanaQuery += " GROUP BY T0.\"ItemCode\", T0.\"ItemName\", T3.\"Father\",  T4.\"UomName\" ";
            HanaQuery += "ORDER BY T0.\"ItemCode\" ";

            oRecordset.DoQuery(HanaQuery);
            if(oRecordset.RecordCount == 0)
            {
                Application.SBO_Application.MessageBox("Attenzione, nessun articolo trovato ");
            }
            this.UIAPIRawForm.Freeze(true);
            this.UIAPIRawForm.DataSources.DataTables.Item("DT_ART").Clear();
            this.UIAPIRawForm.DataSources.DataTables.Item("DT_ART").ExecuteQuery(HanaQuery);
            this.Matrix0.Columns.Item("ItemCode").DataBind.Bind("DT_ART", "ItemCode");
            this.Matrix0.Columns.Item("ItemName").DataBind.Bind("DT_ART", "ItemName");
            this.Matrix0.Columns.Item("DiBa").DataBind.Bind("DT_ART", "Father");
            this.Matrix0.Columns.Item("Unit").DataBind.Bind("DT_ART", "UomName");
            this.Matrix0.LoadFromDataSource();
            this.Matrix0.AutoResizeColumns();
            this.UIAPIRawForm.Freeze(false);
        }

        private void CreaOP()
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            int errore = 0;
            //CHIAMA SEQUENCE
            oRecordset.DoQuery("SELECT MAX( T0.\"U_FO_OPG\") FROM OWOR T0 ");
            int ID_OPG = int.Parse(oRecordset.Fields.Item(0).Value.ToString()) + 1;
            //SE ORDINE STANDARD
            if (this.ComboBox4.Value.ToString() == "std")
            {
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    if (double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString()) > 0)
                    {
                        //Controllo che esistano delle DIBA
                        if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("DiBa").Cells.Item(i).Specific)).Value.ToString() != "")
                        {
                            SAPbobsCOM.ProductionOrders oPO = (SAPbobsCOM.ProductionOrders)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                            
                            double Qta = double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString().Replace(".", ",")); ///DA CONTROLLARE
                            string ItemCode = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("ItemCode").Cells.Item(i).Specific)).Value.ToString();
                            //Estraggo il progetto
                            oPO.ItemNo = ItemCode;
                            oPO.PlannedQuantity = Qta;
                            oPO.DueDate = DateTime.ParseExact(this.EditText3.Value.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                            oPO.UserFields.Fields.Item("U_FO_OPG").Value = ID_OPG;

                            if (this.ComboBox1.Value.ToString() != "")
                            {
                                oPO.ProductionOrderOriginEntry = int.Parse(this.ComboBox1.Value.ToString());
                            }
                            if (this.EditText0.Value.ToString() != "")
                            {
                                oPO.CustomerCode = this.EditText0.Value.ToString();
                            }

                            int linecount = oPO.Lines.Count;

                            int res = oPO.Add();
                            if (res == 0)
                            {

                            }
                            else
                            {
                                Application.SBO_Application.MessageBox("Errore inserimento Ordine Produzione articolo: " + ItemCode + " errore: " + Program.oCompany.GetLastErrorDescription());
                            }
                        }
                        else
                        {
                            errore = 1;
                        }
                    
                    }
                }
                if (errore == 1)
                {
                    Application.SBO_Application.MessageBox("Attenzione, Mancano delle distinte base per gli articoli selezionati");
                }
            }
            else if (this.ComboBox4.Value.ToString() == "spe")
            {
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    if (double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString()) > 0)
                    {
                        SAPbobsCOM.ProductionOrders oPO = (SAPbobsCOM.ProductionOrders)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductionOrders);
                        double Qta = double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString().Replace(".", ","));
                        string ItemCode = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("ItemCode").Cells.Item(i).Specific)).Value.ToString();
                        oPO.ProductionOrderType = SAPbobsCOM.BoProductionOrderTypeEnum.bopotSpecial;
                        oPO.ItemNo = ItemCode;
                        oPO.PlannedQuantity = Qta;
                        
                        if(this.ComboBox1.Value.ToString() != "")
                        {
                            oPO.ProductionOrderOriginEntry = int.Parse(this.ComboBox1.Value.ToString());
                        }
                        if (this.EditText2.Value.ToString() != "")
                        {
                            oPO.CustomerCode = this.EditText2.Value.ToString();
                        }

                        oPO.DueDate = DateTime.ParseExact(this.EditText3.Value.ToString(), "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        oPO.UserFields.Fields.Item("U_FO_OPG").Value = ID_OPG;
                        //Recupero la risorsa
                        oRecordset.DoQuery("SELECT \"Name\" FROM \"@ADDONPAR\" WHERE \"Code\" = 'RISORSA_OP'");

                        //oPO.Stages.StageEntry = 1;
                        //oPO.Stages.SequenceNumber = 1;
                        oPO.Lines.ItemType = SAPbobsCOM.ProductionItemType.pit_Resource;
                        oPO.Lines.ItemNo = oRecordset.Fields.Item(0).Value.ToString(); 
                        //oPO.Lines.StageID = 1;

                        int res = oPO.Add();
                        if (res == 0)
                        { 

                        }
                        else
                        {
                            Application.SBO_Application.MessageBox("Errore inserimento Ordine Produzione articolo: " + ItemCode + " errore: " + Program.oCompany.GetLastErrorDescription());
                            errore = 1;
                        }
                    }
                }
            }
            else
            {
                Application.SBO_Application.MessageBox("Attenzione, Inserire tipo ordine produzione");
                errore = 1;
            }

            if (errore == 0)
            {
                ClearForm();
                Application.SBO_Application.MessageBox("Completato, Elaborazione completata con successo");
            }
        }

        private bool CheckDati()
        {
            if (this.EditText3.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Attenzione, Campo Data Consegna obbligatorio");
                return false;
            }

            if(this.ComboBox4.Value.ToString() == "")
            {
                Application.SBO_Application.MessageBox("Attenzione, Inserire tipo ordine produzione");
                return false;
            }

            if (this.ComboBox4.Value.ToString() == "std")
            {
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    if (double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString()) > 0)
                    {
                        if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("DiBa").Cells.Item(i).Specific)).Value.ToString() == "")
                        {
                            Application.SBO_Application.MessageBox("Attenzione, Mancano delle distinte base per gli articoli selezionati");
                            return false;
                        }
                    }
                }
            }
            bool trovataqta = false;
            for (int i = 1; i <= this.Matrix0.RowCount; i++)
            {
                if (double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Qta").Cells.Item(i).Specific)).Value.ToString()) > 0)
                {
                    trovataqta = true;
                }
            }

            if(!trovataqta)
            {
                Application.SBO_Application.MessageBox("Attenzione, inserire quantità diversa da zero");
                return false;
            }
            return true;
        }

        private void ClearForm()
        {
            this.EditText2.Value = "";
            this.EditText3.Value = "";
            this.ComboBox0.Select("", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            this.ComboBox1.Select("", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            this.ComboBox4.Select("", SAPbouiCOM.BoSearchKey.psk_ByDescription);
            this.Matrix0.Clear();
        }

        private void ComboBox1_KeyDownBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            if (ComboBox0.Value.ToString() != "")
            {
                BubbleEvent = true;
            }
            else
            {
                Application.SBO_Application.SetStatusBarMessage("Selezionare prima il cliente", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                BubbleEvent = false;
            }
        }

        private void ComboBox1_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            PopolaComboSKU();
        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (this.ComboBox0.Value.ToString() == "" && this.ComboBox1.Value.ToString() == "")
            {
                Application.SBO_Application.SetStatusBarMessage("Inserire Ordine o SKU Parent", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
            }
            else
            {
                PopolaMatrixArticoli();
            }
        }

        private void Form_LoadAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (loadFromLink)
            {
                loadFromLink = false;
                PopolaComboSKU();
                condizioniCFLBP();
            }
        }

        private void Form_DataLoadAfter(ref SAPbouiCOM.BusinessObjectInfo pVal)
        {
            loadFromLink = true;
            if (!Object.ReferenceEquals(null, this.Matrix0))
            {
                PopolaComboSKU();
                condizioniCFLBP();
            }

        }

        private void Button1_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            CreaOP();
        }

        private void Button1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = CheckDati();
        }

        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Freeze(true);

            //margini
            int margine_top = 10;
            int margine_bottom = 10;
            int margine_left = 10;
            int margine_right = 10;
            int spaz_vert = 6;
            int spaz_oriz = 10;

            this.StaticText2.Item.Top = margine_top;
            this.StaticText2.Item.Left = margine_left;
            this.EditText0.Item.Left = margine_left + this.StaticText2.Item.Width;
            this.EditText0.Item.Top = margine_top;

            this.StaticText7.Item.Left = this.EditText0.Item.Left + this.EditText0.Item.Width + margine_right + margine_right;
            this.StaticText7.Item.Top = margine_top;
            this.ComboBox1.Item.Top = margine_top;
            this.ComboBox1.Item.Left = this.StaticText7.Item.Left + this.StaticText7.Item.Width;

            this.StaticText4.Item.Top = this.StaticText2.Item.Top + this.StaticText2.Item.Height + margine_top;
            this.StaticText4.Item.Left = margine_left;
            this.ComboBox0.Item.Left = this.EditText0.Item.Left;
            this.ComboBox0.Item.Top = this.StaticText4.Item.Top;

            this.StaticText9.Item.Top = this.StaticText4.Item.Top + this.StaticText4.Item.Height + margine_top;
            this.StaticText9.Item.Left = margine_left;
            this.ComboBox4.Item.Top = this.StaticText9.Item.Top;
            this.ComboBox4.Item.Left = this.EditText0.Item.Left;

            this.EditText3.Item.Top = this.StaticText2.Item.Top;
            this.EditText3.Item.Left = this.UIAPIRawForm.Width - this.EditText3.Item.Width - margine_left - margine_left;
            this.StaticText8.Item.Width = this.StaticText2.Item.Width;
            this.StaticText8.Item.Top = this.StaticText2.Item.Top ;
            this.StaticText8.Item.Left = this.EditText3.Item.Left - this.StaticText8.Item.Width;

            this.Button0.Item.Width = this.StaticText2.Item.Width;
            this.Button0.Item.Top = this.ComboBox4.Item.Top + this.ComboBox4.Item.Height + spaz_vert ;
            this.Button0.Item.Left = this.UIAPIRawForm.Width - this.Button0.Item.Width - margine_left - margine_left;

            this.Matrix0.Item.Top = this.Button0.Item.Top + this.Button0.Item.Height + spaz_vert;
            this.Matrix0.Item.Left = margine_left;
            this.Matrix0.Item.Width = this.UIAPIRawForm.Width - margine_left - margine_left;
            this.Matrix0.AutoResizeColumns();

            this.Button1.Item.Left = margine_left;
            this.Button1.Item.Top = this.Matrix0.Item.Top + this.Matrix0.Item.Height + spaz_vert;
            this.Button1.Item.Left = margine_left;

            this.UIAPIRawForm.Freeze(false);
        }
    
        private void ComboBox1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            if (ComboBox0.Value.ToString() != "")
            {
                BubbleEvent = true;
            }
            else
            {
                Application.SBO_Application.SetStatusBarMessage("Selezionare prima il cliente", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                BubbleEvent = false;
            }
        }

        private void ComboBox0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            if(this.EditText2.Value.ToString() != "" && this.ComboBox1.Value.ToString() == "")
            {
                Application.SBO_Application.SetStatusBarMessage("Selezionare prima un ordine cliente", SAPbouiCOM.BoMessageTime.bmt_Medium, true);
                BubbleEvent = false;
            }
            else
            {
                BubbleEvent = true;
            }
        }

        private void EditText2_ValidateAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            PopolaComboOrdini();

        }

        private void EditText2_DoubleClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            ListaClienti activeForm = new ListaClienti(this.UIAPIRawForm.TypeCount);
            activeForm.Show();
        }

        private void ComboBox2_ComboSelectAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            PopolaComboOrdini();
        }


        private SAPbouiCOM.EditText EditText0;

        private void EditText0_ValidateAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            PopolaComboOrdini();

        }

        private void EditText0_ChooseFromListAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            SAPbouiCOM.ISBOChooseFromListEventArg chflarg = (SAPbouiCOM.ISBOChooseFromListEventArg)pVal;
            SAPbouiCOM.DataTable dt = chflarg.SelectedObjects;

            if (dt != null)
            {
                string cardCode = chflarg.SelectedObjects.GetValue("CardCode", 0).ToString();

                this.UIAPIRawForm.DataSources.UserDataSources.Item("UD_BP2").Value = cardCode;
            }
            else
            {
                if (this.EditText0.Value == "")
                {
                    this.EditText0.Value = "";
                }
            }

        }
    }
}
