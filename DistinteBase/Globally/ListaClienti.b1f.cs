﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace DistinteBase
{
    [FormAttribute("DistinteBase.ListaClienti", "ListaClienti.b1f")]
    class ListaClienti : UserFormBase
    {
        private int typeCount;
        public ListaClienti(int TypeCountP)
        {
            typeCount = TypeCountP;
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Grid0 = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.Grid0.DoubleClickAfter += new SAPbouiCOM._IGridEvents_DoubleClickAfterEventHandler(this.Grid0_DoubleClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Grid Grid0;

        private void OnCustomInitialize()
        {

        }

        private void Grid0_DoubleClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (pVal.Row != -1)
            {
                string uid = this.UIAPIRawForm.UniqueID;
                SAPbouiCOM.Form oItemForm = Application.SBO_Application.Forms.GetForm("DistinteBase.DiBa", typeCount);
                SAPbouiCOM.EditText oEdit = (SAPbouiCOM.EditText)oItemForm.Items.Item("Item_1").Specific;
                oEdit.Value = this.Grid0.DataTable.GetValue("Codice Cliente", pVal.Row).ToString();
                
                this.UIAPIRawForm.Close();
            }
        }
    }
}
