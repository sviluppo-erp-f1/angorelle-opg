﻿using System;
using System.Collections.Generic;
using System.Text;
using SAPbouiCOM.Framework;

namespace DistinteBase
{
    class Menu
    {
        public void AddMenuItems()
        {
            SAPbouiCOM.Menus oMenus = null;
            SAPbouiCOM.MenuItem oMenuItem = null;

            oMenus = Application.SBO_Application.Menus;

            SAPbouiCOM.MenuCreationParams oCreationPackage = null;
            oCreationPackage = ((SAPbouiCOM.MenuCreationParams)(Application.SBO_Application.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)));
            oMenuItem = Application.SBO_Application.Menus.Item("43520"); // moudles'

            oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_POPUP;
            oCreationPackage.UniqueID = "DistinteBase";
            oCreationPackage.String = "DistinteBase";
            oCreationPackage.Enabled = true;
            oCreationPackage.Position = -1;

            oMenus = oMenuItem.SubMenus;

            #region Distinte Base
            oMenuItem = Application.SBO_Application.Menus.Item("4352"); // Produzione -> Distinte Base
            try
            {
                oMenus = oMenuItem.SubMenus;
                // Create s sub menu
                oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING;
                oCreationPackage.UniqueID = "DistinteBase.DiBa";
                oCreationPackage.String = "Ordine Prod. di Gruppo";
                oCreationPackage.Position = -1;
                oMenus.AddEx(oCreationPackage);
            }
            catch (Exception)
            {
                //  Menu already exists
                oMenus.RemoveEx("DistinteBase.DiBa");
                oMenus.AddEx(oCreationPackage);
            }

            #endregion

         
        }

        public void SBO_Application_MenuEvent(ref SAPbouiCOM.MenuEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;

            try
            {
                if (pVal.BeforeAction && pVal.MenuUID == "DistinteBase.DiBa")
                {
                    try
                    {
                        Application.SBO_Application.Forms.Item("DiBa").Select();
                    }
                    catch (Exception)
                    {
                        DiBa activeForm = new DiBa();
                        activeForm.Show();
                    }
                }

            }
            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox(ex.ToString(), 1, "Ok", "", "");
            }
        }

    }
}
