﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace DistinteBase
{
    [FormAttribute("DistinteBase.Pesi", "Pesi.b1f")]
    class Pesi : UserFormBase
    {
        int TypeCount;
        string listaArticoli;
        int docEntry;
        public Pesi(int TypeCountP, string listaArticoliP, int docEntryP)
        {
            TypeCount = TypeCountP;
            listaArticoli = listaArticoliP;
            docEntry = docEntryP;

            popolaMatrix();
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("Item_0").Specific));
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("1").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Matrix Matrix0;

        private void OnCustomInitialize()
        {
            this.Matrix0.Columns.Item("DocEntry").Visible = false;
            this.Matrix0.Columns.Item("Lav").Editable = false;
            popolaComboLavaggio();
        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;

        public void popolaMatrix()
        {
            //Costruisco la condizione di estrazione delle informazioni da tabella
            SAPbouiCOM.Conditions oConds = new SAPbouiCOM.Conditions();
            SAPbouiCOM.Condition oCond;
            oCond = oConds.Add();
            oCond.Alias = "U_FO_DOCENTRY";
            oCond.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL;
            oCond.CondVal = docEntry.ToString();

            //Carico la griglia
            SAPbouiCOM.DBDataSource oDBDataSource = this.UIAPIRawForm.DataSources.DBDataSources.Item("@FO_STORICOLAVAGGIO");
            oDBDataSource.Query(oConds);
            this.Matrix0.LoadFromDataSource();
            this.Matrix0.AutoResizeColumns();



        }

        private void popolaComboLavaggio()
        {
            SAPbouiCOM.Column oCBC = this.Matrix0.Columns.Item("Lav");
            if (oCBC.ValidValues.Count == 0)
            {
                oCBC.ValidValues.Add("", "");

                SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("SELECT \"Code\", \"Name\" FROM \"@FO_EC_LAVAGGIO\" ");
                while (!oRecordset.EoF)
                {
                    oCBC.ValidValues.Add(oRecordset.Fields.Item(0).Value.ToString(), oRecordset.Fields.Item(1).Value.ToString());
                    oRecordset.MoveNext();
                }
                oCBC.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                oCBC.DisplayDesc = true;
            }
        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            for (int i = 1; i <= this.Matrix0.RowCount; i++)
            {
                try
                {
                    string peso = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("Peso").Cells.Item(i).Specific)).Value.ToString().Replace(",", ".");
                    if (peso == "")
                    {
                        peso = "0";
                    }
                    string tipo = ((SAPbouiCOM.ComboBox)(this.Matrix0.Columns.Item("Lav").Cells.Item(i).Specific)).Value.ToString();
                    oRecordset.DoQuery("UPDATE \"@FO_STORICOLAVAGGIO\" SET U_FO_QUANTITA = " + peso + " WHERE U_FO_TIPOLAVAGGIO = '" + tipo + "' AND U_FO_DOCENTRY = " + docEntry + " ");
                }
                catch (Exception e)
                {
                    Application.SBO_Application.MessageBox(e.Message + " - " + e.ToString());
                }
            }
            
        }
    }
}
