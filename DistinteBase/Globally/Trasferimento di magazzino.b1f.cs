
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;
using System.Net.Sockets;
using System.IO;

namespace DistinteBase
{

    [FormAttribute("940", "Trasferimento di magazzino.b1f")]
    class Trasferimento_di_magazzino : SystemFormBase
    {
        public Trasferimento_di_magazzino()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("Item_0").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Matrix0 = ((SAPbouiCOM.Matrix)(this.GetItem("23").Specific));
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("41").Specific));
            this.Folder0 = ((SAPbouiCOM.Folder)(this.GetItem("Item_1").Specific));
            this.Folder0.ClickAfter += new SAPbouiCOM._IFolderEvents_ClickAfterEventHandler(this.Folder0_ClickAfter);
            this.StaticText1 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_3").Specific));
            this.StaticText2 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_4").Specific));
            this.StaticText3 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_5").Specific));
            this.StaticText4 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_6").Specific));
            this.StaticText5 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_7").Specific));
            this.StaticText6 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_8").Specific));
            this.StaticText7 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_9").Specific));
            this.StaticText8 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_10").Specific));
            this.StaticText9 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_11").Specific));
            this.EditText1 = ((SAPbouiCOM.EditText)(this.GetItem("Item_12").Specific));
            this.EditText2 = ((SAPbouiCOM.EditText)(this.GetItem("Item_13").Specific));
            this.EditText4 = ((SAPbouiCOM.EditText)(this.GetItem("Item_15").Specific));
            this.EditText5 = ((SAPbouiCOM.EditText)(this.GetItem("Item_16").Specific));
            this.EditText6 = ((SAPbouiCOM.EditText)(this.GetItem("Item_17").Specific));
            this.EditText7 = ((SAPbouiCOM.EditText)(this.GetItem("Item_18").Specific));
            this.EditText8 = ((SAPbouiCOM.EditText)(this.GetItem("Item_19").Specific));
            this.EditText9 = ((SAPbouiCOM.EditText)(this.GetItem("Item_20").Specific));
            this.ComboBox0 = ((SAPbouiCOM.ComboBox)(this.GetItem("Item_21").Specific));
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("Item_2").Specific));
            this.Button2.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button2_ClickAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.DataAddAfter += new SAPbouiCOM.Framework.FormBase.DataAddAfterHandler(this.Form_DataAddAfter);
            this.ResizeAfter += new SAPbouiCOM.Framework.FormBase.ResizeAfterHandler(this.Form_ResizeAfter);
            this.LoadAfter += new LoadAfterHandler(this.Form_LoadAfter);

        }

        #region Elementi
        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Matrix Matrix0;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.EditText EditText0;
        private SAPbouiCOM.Folder Folder0;
        private SAPbouiCOM.StaticText StaticText1;
        private SAPbouiCOM.StaticText StaticText2;
        private SAPbouiCOM.StaticText StaticText3;
        private SAPbouiCOM.StaticText StaticText4;
        private SAPbouiCOM.StaticText StaticText5;
        private SAPbouiCOM.StaticText StaticText6;
        private SAPbouiCOM.StaticText StaticText7;
        private SAPbouiCOM.StaticText StaticText8;
        private SAPbouiCOM.StaticText StaticText9;
        private SAPbouiCOM.EditText EditText1;
        private SAPbouiCOM.EditText EditText2;
        private SAPbouiCOM.EditText EditText4;
        private SAPbouiCOM.EditText EditText5;
        private SAPbouiCOM.EditText EditText6;
        private SAPbouiCOM.EditText EditText7;
        private SAPbouiCOM.EditText EditText8;
        private SAPbouiCOM.EditText EditText9;
        private SAPbouiCOM.ComboBox ComboBox0;
        #endregion 

        private void OnCustomInitialize()
        {
            SetBound();
        }

        private void SetBound()
        {
            this.EditText1.DataBind.SetBound(true, "OWTR", "U_FO_EC_CAUSALE");
            this.EditText2.DataBind.SetBound(true, "OWTR", "U_FO_VETTORE");
            this.ComboBox0.DataBind.SetBound(true, "OWTR", "U_Porto");
            this.EditText4.DataBind.SetBound(true, "OWTR", "U_FO_N_COLLI");
            this.EditText5.DataBind.SetBound(true, "OWTR", "U_FO_ORA_TRASPORTO");
            this.EditText6.DataBind.SetBound(true, "OWTR", "U_FO_DATA_TRASPORTO");
            this.EditText7.DataBind.SetBound(true, "OWTR", "U_FO_PESONETTO");
            this.EditText8.DataBind.SetBound(true, "OWTR", "U_FO_PESOLORDO");
            this.EditText9.DataBind.SetBound(true, "OWTR", "U_FO_DESC_IMBALLAGGIO");
        }

        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (this.UIAPIRawForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE)
            {
                string listaArticoli = "";
                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("1").Cells.Item(i).Specific)).Value.ToString() != "")
                    {
                        if (listaArticoli != "")
                        {
                            listaArticoli = listaArticoli + ",";
                        }
                        listaArticoli = listaArticoli + "'" + ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("1").Cells.Item(i).Specific)).Value.ToString() + "'";
                    }
                }

                string parLista = "(" + listaArticoli + ")";
                Pesi activeForm = new Pesi(this.UIAPIRawForm.TypeCount, parLista, int.Parse(this.UIAPIRawForm.DataSources.DBDataSources.Item("OWTR").GetValue("DocEntry", 0).ToString()));
                activeForm.Show();
            }
            else
            {
                Application.SBO_Application.MessageBox("E' necessario salvare i dati prima di inserire i pesi.");
            }

        }

        private void Form_DataAddAfter(ref SAPbouiCOM.BusinessObjectInfo pVal)
        {
            if (!pVal.ActionSuccess)
            {
                return;
            }

            string docEntry = "";
            string objectKey = pVal.ObjectKey;
            int pos_ini = objectKey.IndexOf("<DocEntry>") + 10;
            int pos_fin = objectKey.IndexOf("</DocEntry>");
            docEntry = objectKey.Substring(pos_ini, pos_fin - pos_ini);
            if (docEntry == "")
            {
                return;
            }

            inserisciRighePeso(int.Parse(docEntry));

        }

        private void inserisciRighePeso(int docEntry)
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string sqlQuery = "";
            sqlQuery = "INSERT INTO \"@FO_STORICOLAVAGGIO\" (\"Code\",U_FO_DOCENTRY, U_FO_OBJTYPE, U_FO_TIPOLAVAGGIO) ";
            sqlQuery = sqlQuery + "( ";
            sqlQuery = sqlQuery + "SELECT \"@FO_STORICOLAVAGGIO_S\".nextval, aa.* FROM ( ";
            sqlQuery = sqlQuery + "SELECT  distinct T0.\"DocEntry\", T3.\"ObjType\", T2.\"Code\" ";
            sqlQuery = sqlQuery + "FROM    WTR1 T0 ";
            sqlQuery = sqlQuery + "            INNER JOIN OITM T1 ON T0.\"ItemCode\" = T1.\"ItemCode\" ";
            sqlQuery = sqlQuery + "            INNER JOIN \"@FO_EC_LAVAGGIO\"  T2 ON T1.\"U_FO_EC_LAVAGGIO\" = T2.\"Code\" ";
            sqlQuery = sqlQuery + "            INNER JOIN OWTR T3 ON T0.\"DocEntry\" = T3.\"DocEntry\" ";
            sqlQuery = sqlQuery + "            INNER JOIN OWHS T4 ON T0.\"WhsCode\" = T4.\"WhsCode\" ";
            sqlQuery = sqlQuery + "WHERE   T4.U_FO_EC_LAV_ESTERNA='Y' AND T0.\"DocEntry\" = " + docEntry;
            sqlQuery = sqlQuery + ") aa ";
            sqlQuery = sqlQuery + ") ";

            oRecordset.DoQuery(sqlQuery);
        }

        private void popolaComboPorto()
        {
            SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //SAPbouiCOM.Column oCBC = this.Matrix0.Columns.Item("U_F1_UpdateList");
            if (this.ComboBox0.ValidValues.Count == 0)
            {
                this.ComboBox0.ValidValues.Add("", "");

                oRecordset.DoQuery("SELECT \"Code\", \"Name\" FROM \"@FO_PORTO\" ");
                while (!oRecordset.EoF)
                {
                    this.ComboBox0.ValidValues.Add(oRecordset.Fields.Item(0).Value.ToString(), oRecordset.Fields.Item(1).Value.ToString());
                    oRecordset.MoveNext();
                }
                this.ComboBox0.ExpandType = SAPbouiCOM.BoExpandType.et_DescriptionOnly;
                //this.ComboBox0.DisplayDesc = true;
            }
        }

        private void Form_ResizeAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.Button0.Item.Height = this.Button1.Item.Height;
            this.Button0.Item.Top = this.Button1.Item.Top;
            this.Button0.Item.Width = this.Button1.Item.Width;
            this.Button0.Item.Left = this.EditText0.Item.Left + this.EditText0.Item.Width;


            this.Button2.Item.Top = this.Button0.Item.Top;
            this.Button2.Item.Width = this.Button0.Item.Width + this.Button0.Item.Left + 30;

            #region TAB DETTAGLI
            int MatrixTop = this.UIAPIRawForm.Items.Item("23").Top + 50;
            int MatrixLeft = this.UIAPIRawForm.Items.Item("23").Left;

            int LarghezzaCampi = 150;
            int SpaziaturaSmall = 10;
            int SpaziaturaLarge = 100;
            int SpaziaturaOrizz = 70;

            this.StaticText1.Item.Width = this.StaticText2.Item.Width = this.StaticText3.Item.Width = this.StaticText4.Item.Width = this.StaticText5.Item.Width = LarghezzaCampi;
            this.StaticText6.Item.Width = this.StaticText7.Item.Width = this.StaticText8.Item.Width = this.StaticText9.Item.Width = this.StaticText1.Item.Width;

            this.EditText1.Item.Width = this.EditText2.Item.Width = this.ComboBox0.Item.Width = this.EditText4.Item.Width = this.EditText5.Item.Width = LarghezzaCampi;
            this.EditText6.Item.Width = this.EditText7.Item.Width = this.EditText8.Item.Width = this.EditText9.Item.Width = this.EditText1.Item.Width;

            this.StaticText1.Item.Left = MatrixLeft;
            this.StaticText1.Item.Top = MatrixTop + 3;
            this.EditText1.Item.Left = this.StaticText1.Item.Left + this.StaticText1.Item.Width;
            this.EditText1.Item.Top = this.StaticText1.Item.Top;

            this.StaticText2.Item.Left = MatrixLeft;
            this.StaticText2.Item.Top = this.StaticText1.Item.Top + this.StaticText1.Item.Height + SpaziaturaSmall;
            this.EditText2.Item.Left = this.StaticText2.Item.Left + this.StaticText2.Item.Width;
            this.EditText2.Item.Top = this.StaticText2.Item.Top;

            this.StaticText3.Item.Left = MatrixLeft;
            this.StaticText3.Item.Top = this.StaticText2.Item.Top + this.StaticText1.Item.Height + SpaziaturaSmall;
            this.ComboBox0.Item.Left = this.StaticText3.Item.Left + this.StaticText3.Item.Width;
            this.ComboBox0.Item.Top = this.StaticText3.Item.Top;

            this.StaticText4.Item.Left = MatrixLeft;
            this.StaticText4.Item.Top = this.StaticText3.Item.Top + this.StaticText1.Item.Height + SpaziaturaLarge;
            this.EditText4.Item.Left = this.StaticText4.Item.Left + this.StaticText4.Item.Width;
            this.EditText4.Item.Top = this.StaticText4.Item.Top;

            this.StaticText5.Item.Left = MatrixLeft;
            this.StaticText5.Item.Top = this.StaticText4.Item.Top + this.StaticText1.Item.Height + SpaziaturaLarge;
            this.EditText5.Item.Left = this.StaticText5.Item.Left + this.StaticText5.Item.Width;
            this.EditText5.Item.Top = this.StaticText5.Item.Top;

            this.StaticText6.Item.Left = MatrixLeft;
            this.StaticText6.Item.Top = this.StaticText5.Item.Top + this.StaticText1.Item.Height + SpaziaturaSmall;
            this.EditText6.Item.Left = this.StaticText6.Item.Left + this.StaticText6.Item.Width;
            this.EditText6.Item.Top = this.StaticText6.Item.Top;

            this.StaticText7.Item.Left = this.EditText0.Item.Left + this.EditText0.Item.Width + SpaziaturaOrizz;
            this.StaticText7.Item.Top = this.StaticText1.Item.Top;
            this.EditText7.Item.Left = this.StaticText7.Item.Left + this.StaticText7.Item.Width;
            this.EditText7.Item.Top = this.StaticText7.Item.Top;

            this.StaticText8.Item.Left = this.StaticText7.Item.Left;
            this.StaticText8.Item.Top = this.StaticText2.Item.Top;
            this.EditText8.Item.Left = this.StaticText8.Item.Left + this.StaticText8.Item.Width;
            this.EditText8.Item.Top = this.StaticText8.Item.Top;

            this.StaticText9.Item.Left = this.StaticText7.Item.Left;
            this.StaticText9.Item.Top = this.StaticText3.Item.Top;
            this.EditText9.Item.Left = this.StaticText9.Item.Left + this.StaticText9.Item.Width;
            this.EditText9.Item.Top = this.StaticText9.Item.Top;
            #endregion
        }

        private void posizionaTab(SAPbouiCOM.Folder oFolder, string ObjectID)
        {
            oFolder.Item.Left = ((SAPbouiCOM.Folder)(this.GetItem(ObjectID).Specific)).Item.Left + ((SAPbouiCOM.Folder)(this.GetItem(ObjectID).Specific)).Item.Width;
            oFolder.Item.Width = 50;// ((SAPbouiCOM.Folder)(this.GetItem(ObjectID).Specific)).Item.Width;
            oFolder.GroupWith(ObjectID);
        }

        private void Folder0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.PaneLevel = 10;
            popolaComboPorto();
        }

        private void Form_LoadAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {
            posizionaTab(this.Folder0, "1980000001");
            this.Folder0.Item.LinkTo = "1980000001";
        }

        private SAPbouiCOM.Button Button2;

        private void Button2_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (this.UIAPIRawForm.Mode == SAPbouiCOM.BoFormMode.fm_OK_MODE)
            {
                SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)Program.oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("SELECT \"Code\",\"Name\" FROM \"@ADDONPAR\" WHERE \"Code\" IN ( 'FO_ZEBRA_IP' , 'FO_ZEBRA_PORT' )");
                string IP = "";
                int port = 0;
                while (!oRecordset.EoF)
                {
                    if (oRecordset.Fields.Item(0).Value.ToString() == "FO_ZEBRA_IP") IP = oRecordset.Fields.Item("Name").Value.ToString();
                    if (oRecordset.Fields.Item(0).Value.ToString() == "FO_ZEBRA_PORT") port = int.Parse(oRecordset.Fields.Item("Name").Value.ToString());
                    oRecordset.MoveNext();
                }

                for (int i = 1; i <= this.Matrix0.RowCount; i++)
                {
                    
                    if (((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("1").Cells.Item(i).Specific)).Value.ToString() != "")
                    {
                        double qta = double.Parse(((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("10").Cells.Item(i).Specific)).Value.ToString().Replace(".",","));
                        string articolo = ((SAPbouiCOM.EditText)(this.Matrix0.Columns.Item("1").Cells.Item(i).Specific)).Value.ToString();
                        string sqlQuery = "";
                        sqlQuery += "SELECT a.U_FO_EC_DESCSKU, ";
                        sqlQuery += "       a.U_FO_EC_SKU, ";
                        sqlQuery += "       c.\"Name\" AS \"U_FO_EC_TAGLIA\", ";
                        sqlQuery += "       b.U_FO_EC_DESC ";
                        sqlQuery += "FROM   OITM a ";
                        sqlQuery += "           INNER JOIN \"@FO_EC_COLORI_FILATO\" b ON a.U_FO_EC_COLORI = b.\"Code\" ";
                        sqlQuery += "           INNER JOIN \"@FO_EC_TAGLIA\" c ON a.U_FO_EC_TAGLIA = c.\"Code\" ";
                        sqlQuery += "WHERE  \"ItemCode\" = '" + articolo + "' ";

                        oRecordset.DoQuery(sqlQuery);
                        string descrizione = oRecordset.Fields.Item("U_FO_EC_DESCSKU").Value.ToString();
                        string infoSKU = oRecordset.Fields.Item("U_FO_EC_SKU").Value.ToString() + " / " + oRecordset.Fields.Item("U_FO_EC_TAGLIA").Value.ToString() + " / " + oRecordset.Fields.Item("U_FO_EC_DESC").Value.ToString();

                        for (int x = 0; x < qta; x++)
                        {
                            stampaEtichetta(IP, port, descrizione, infoSKU, articolo);
                        }
                        

                    }
                }

            }
            else
            {
                Application.SBO_Application.MessageBox("E' necessario salvare i dati prima di stampare le etichette.");
            }

        }


        private void stampaEtichetta(string IP, int port, string descrizione, string infoSKU, string articolo)
        {
            System.Net.Sockets.TcpClient zebraClient = new System.Net.Sockets.TcpClient();
            try
            {
                zebraClient.SendTimeout = 5000;
                zebraClient.Connect(IP, port);
            }
            catch (Exception ex)
            {
                Application.SBO_Application.MessageBox("Impossibile connettersi alla stampante Zebra.");
            }

            if (zebraClient.Connected)
            {
                NetworkStream nStream;
                nStream = zebraClient.GetStream();
                StreamWriter wStream;
                using (nStream)
                {
                    wStream = new StreamWriter(nStream);
                    using (wStream)
                    {
                        StringBuilder stream = new StringBuilder();

                        stream.Append("^XA");

                        stream.Append("^FX BARCODE");
                        stream.Append("^CFA,35");
                        stream.Append("^FO0,30^A0N,25,25^FB500,1,0,C^FD");
                        stream.Append(descrizione);
                        stream.Append("^FS");

                        stream.Append("^FX BARCODE");
                        stream.Append("^CFA,35");
                        stream.Append("^FO0,80^A0N,25,25^FB500,1,0,C^FD");
                        stream.Append(infoSKU);
                        stream.Append("^FS");


                        stream.Append("^FX BARCODE");
                        stream.Append("^BY1,2,80");
                        stream.Append("^FO125,130^FB405,1,0,C^BC^FD");
                        stream.Append(articolo);
                        stream.Append("^FS");

                        stream.Append("^XZ");

                        wStream.Write(stream.ToString());
                        wStream.Flush();
                        wStream.Close();


                    }
                }
                zebraClient.Close();
            }

        }
    }
}
